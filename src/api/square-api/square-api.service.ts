import { HttpException, Injectable } from '@nestjs/common';
import { Merchant } from '../../merchant/merchant';
import { ProductDto } from '../../product/dto/product-dto';
import { HttpService } from '@nestjs/axios';
import { catchError, map, Observable } from 'rxjs';
import { SquareProductResponse } from './square-product-response';
import { SquareApiMapperService } from './square-api-mapper.service';

@Injectable()
export class SquareApiService {
  public static readonly MERCHANT_NAME = 'square';

  constructor(
    private readonly httpService: HttpService,
    private readonly squareApiMapperService: SquareApiMapperService,
  ) {}

  findProduct(merchant: Merchant, productId: string): Observable<ProductDto> {
    return this.httpService
      .get<SquareProductResponse>(
        'https://connect.squareupsandbox.com/v2/catalog/object/' + productId,
        this.getConfig(merchant),
      )
      .pipe(
        map((response) =>
          this.squareApiMapperService.buildProduct(response.data),
        ),
        catchError((e) => {
          throw new HttpException(e?.response?.data, e?.response?.status);
        }),
      );
  }

  private getConfig(merchant: Merchant) {
    const bearer = 'Bearer ' + merchant.platform.auth.token;
    return {
      headers: {
        'Square-Version': '2021-03-17',
        Authorization: bearer,
        'Content-Type': 'application/json',
      },
    };
  }
}
