import { Test, TestingModule } from '@nestjs/testing';
import { SquareApiService } from './square-api.service';
import { HttpModule, HttpService } from '@nestjs/axios';
import { SquareApiMapperService } from './square-api-mapper.service';
import { ApiModule } from '../api.module';
import { AxiosResponse } from 'axios';
import { of, throwError } from 'rxjs';
import { SquareProductResponse } from './square-product-response';
import { ProductDto } from '../../product/dto/product-dto';

describe('SquareApiService', () => {
  let squareApiService: SquareApiService;
  let httpService: HttpService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ApiModule, HttpModule],
      providers: [SquareApiService, SquareApiMapperService],
    }).compile();

    squareApiService = module.get<SquareApiService>(SquareApiService);
    httpService = module.get<HttpService>(HttpService);
  });

  describe('When find product', () => {
    it('Should make a proper GET Request to Square API ', () => {
      const response: AxiosResponse<SquareProductResponse> = {
        config: undefined,
        headers: undefined,
        status: 201,
        statusText: '',
        data: {},
      };
      const httpServiceSpy = jest
        .spyOn(httpService, 'get')
        .mockImplementation(() => of(response));
      squareApiService.findProduct(
        squareMerchant,
        squareMerchant.products[0].id,
      );
      expect(httpServiceSpy).toBeCalledWith(
        expectedUrl,
        getExpectedSquareApiConfig(),
      );
      expect(httpServiceSpy).toBeCalledTimes(1);
    });
    it('Should map the Square product to a ProductDTO ', (done) => {
      //This test should be done in square-mapper.service.spec
      const response: AxiosResponse<SquareProductResponse> = {
        config: undefined,
        headers: undefined,
        status: 201,
        statusText: '',
        data: squareProductResponse,
      };
      jest.spyOn(httpService, 'get').mockImplementation(() => of(response));
      squareApiService
        .findProduct(squareMerchant, squareMerchant.products[0].id)
        .subscribe((squareProductDto) => {
          expect(squareProductDto.id).toBe(
            squareProductResponse.object.id.toString(),
          );
          expect(squareProductDto.title).toBe(
            squareProductResponse.object.item_data.name,
          );
          expect(squareProductDto.description).toBe(
            squareProductResponse.object.item_data.description,
          );
          expect(squareProductDto.id).toBe(squareProductResponse.object.id);
          expect(squareProductDto.merchantName).toBe(
            SquareApiService.MERCHANT_NAME,
          );
          expect(squareProductDto.variants[0].id).toBe(
            squareProductResponse.object.item_data.variations[0].id,
          );
          expect(squareProductDto.variants[0].price).toBe(
            squareProductResponse.object.item_data.variations[0].item_variation_data.price_money.amount.toString(),
          );
          expect(squareProductDto.variants[0].title).toBe(
            squareProductResponse.object.item_data.variations[0]
              .item_variation_data.name,
          );
          done();
        });
    });
    it('Should handle Square API errors', (done) => {
      const result = {
        response: {
          data: '',
          status: 404,
          statusText: 'string',
          headers: {},
          config: {},
        },
      };
      jest
        .spyOn(httpService, 'get')
        .mockImplementation(() => throwError(() => result));
      squareApiService
        .findProduct(squareMerchant, squareMerchant.products[0].id)
        .subscribe({
          error: (err) => {
            expect(err.status).toBe(result.response.status);
            done();
          },
        });
    });
  });
});
export const productDto: ProductDto = {
  id: '1',
  variants: [],
  title: '',
  merchantName: '',
  description: '',
};
export const squareMerchant = {
  merchant: 'Merchant 1',
  platform: {
    type: SquareApiService.MERCHANT_NAME,
    auth: {
      token: 'a token',
    },
  },
  products: [
    {
      id: productDto.id,
    },
  ],
};
export const expectedUrl =
  'https://connect.squareupsandbox.com/v2/catalog/object/' + productDto.id;

export function getExpectedSquareApiConfig() {
  const bearer = 'Bearer ' + squareMerchant.platform.auth.token;
  return {
    headers: {
      'Square-Version': '2021-03-17',
      Authorization: bearer,
      'Content-Type': 'application/json',
    },
  };
}

export const squareProductResponse = {
  object: {
    type: 'string',
    id: productDto.id,
    updated_at: 'string',
    created_at: 'string',
    version: 1,
    is_deleted: false,
    item_data: {
      name: 'name',
      description: 'des',
      variations: [
        {
          type: 'string',
          id: 'string',
          updated_at: 'string',
          created_at: 'string',
          item_variation_data: {
            item_id: 'string',
            name: 'var name',
            price_money: {
              amount: 5,
              currency: 'string',
            },
          },
        },
      ],
    },
  },
};
