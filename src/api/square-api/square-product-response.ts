export class SquareProductResponse {
  object?: {
    type: string;
    id: string;
    updated_at: string;
    created_at: string;
    version: number;
    is_deleted: boolean;
    item_data: {
      name: string;
      description: string;
      variations: SquareVariantResponse[];
    };
  };
}

export class SquareVariantResponse {
  type: string;
  id: string;
  updated_at: string;
  created_at: string;
  item_variation_data: {
    item_id: string;
    name: string;
    price_money: {
      amount: number;
      currency: string;
    };
  };
}
