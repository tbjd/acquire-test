import { Injectable } from '@nestjs/common';
import {
  SquareProductResponse,
  SquareVariantResponse,
} from './square-product-response';
import { ProductDto } from '../../product/dto/product-dto';
import { SquareApiService } from './square-api.service';

@Injectable()
export class SquareApiMapperService {
  buildProduct(squareProductResponse: SquareProductResponse): ProductDto {
    return {
      id: squareProductResponse.object.id,
      description: squareProductResponse.object.item_data.description,
      merchantName: SquareApiService.MERCHANT_NAME,
      title: squareProductResponse.object.item_data.name,
      variants: this.getVariants(
        squareProductResponse.object.item_data.variations,
      ),
    };
  }

  private getVariants(squareVariantResponses: SquareVariantResponse[]) {
    return squareVariantResponses.map((squareVariant) => {
      return {
        id: squareVariant.id,
        title: squareVariant.item_variation_data.name,
        price: squareVariant.item_variation_data.price_money.amount.toString(),
      };
    });
  }
}
