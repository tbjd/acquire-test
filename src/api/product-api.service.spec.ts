import { Test, TestingModule } from '@nestjs/testing';
import { ProductApiService } from './product-api.service';
import { ApiModule } from './api.module';
import { ShopifyApiService } from './shopify-api/shopify-api.service';
import { SquareApiService } from './square-api/square-api.service';
import { of } from 'rxjs';
import { ProductDto } from '../product/dto/product-dto';
import { HttpModule } from '@nestjs/axios';
import { ShopifyMapperService } from './shopify-api/shopify-mapper.service';
import { SquareApiMapperService } from './square-api/square-api-mapper.service';
import { NotImplementedException } from '@nestjs/common';

describe('ProductApiService', () => {
  let productApiService: ProductApiService;
  let shopifyApiService: ShopifyApiService;
  let squareApiService: SquareApiService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ApiModule, HttpModule],
      providers: [
        ProductApiService,
        ShopifyApiService,
        SquareApiService,
        ShopifyMapperService,
        SquareApiMapperService,
      ],
    }).compile();

    productApiService = module.get<ProductApiService>(ProductApiService);
    shopifyApiService = module.get<ShopifyApiService>(ShopifyApiService);
    squareApiService = module.get<SquareApiService>(SquareApiService);
  });
  describe('When find product', () => {
    describe('And merchant is Shopify', () => {
      it('Should call Shopify API', async () => {
        const shopifyApiServiceSpy = jest
          .spyOn(shopifyApiService, 'findProduct')
          .mockImplementation(() => of(productDto));

        productApiService.findProduct(
          shopifyMerchant,
          shopifyMerchant.products[0].id,
        );

        expect(shopifyApiServiceSpy).toBeCalledTimes(1);
      });
    });
    describe('And merchant is Square', () => {
      it('Should call Square API', () => {
        const squareApiServiceSpy = jest
          .spyOn(squareApiService, 'findProduct')
          .mockImplementation(() => of(productDto));

        productApiService.findProduct(
          squareMerchant,
          squareMerchant.products[0].id,
        );

        expect(squareApiServiceSpy).toBeCalledTimes(1);
      });
    });
    describe('And merchant is unknown', () => {
      it('Should throw error ', () => {
        expect(() =>
          productApiService.findProduct(unknownMerchant, 'an_id'),
        ).toThrow(NotImplementedException);
      });
    });
  });
});
export const productDto: ProductDto = {
  id: '1',
  variants: [],
  title: '',
  merchantName: '',
  description: '',
};
export const shopifyMerchant = {
  merchant: 'Merchant 1',
  platform: {
    type: ShopifyApiService.MERCHANT_NAME,
    auth: {
      key: 'a_key',
      password: 'a_password',
    },
  },
  products: [
    {
      id: productDto.id,
    },
  ],
};
export const squareMerchant = {
  merchant: 'Merchant 2',
  platform: {
    type: SquareApiService.MERCHANT_NAME,
    auth: {
      token: 'a_token',
    },
  },
  products: [
    {
      id: productDto.id,
    },
  ],
};
export const unknownMerchant = {
  merchant: 'Merchant 2',
  platform: {
    type: 'not supported',
    auth: {
      token: 'a_token',
    },
  },
  products: [
    {
      id: productDto.id,
    },
  ],
};
