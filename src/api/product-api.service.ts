import { Injectable, NotImplementedException } from '@nestjs/common';
import { Merchant } from '../merchant/merchant';
import { ProductDto } from '../product/dto/product-dto';
import { SquareApiService } from './square-api/square-api.service';
import { ShopifyApiService } from './shopify-api/shopify-api.service';
import { Observable } from 'rxjs';

@Injectable()
export class ProductApiService {
  constructor(
    private readonly shopifyApiService: ShopifyApiService,
    private readonly squareApiService: SquareApiService,
  ) {}

  findProduct(merchant: Merchant, productId: string): Observable<ProductDto> {
    switch (merchant.platform.type) {
      case ShopifyApiService.MERCHANT_NAME:
        return this.shopifyApiService.findProduct(merchant, productId);
      case SquareApiService.MERCHANT_NAME:
        return this.squareApiService.findProduct(merchant, productId);
      default:
        throw new NotImplementedException(
          `Merchant Type: ${merchant.platform.type} is not supported`,
        );
    }
  }
}
