export class ShopifyProductResponse {
  product?: {
    id: number;
    title: string;
    body_html: string;
    vendor: string;
    product_type: string;
    created_at: string;
    handle: string;
    updated_at: string;
    published_at: string;
    template_suffix: string;
    status: string;
    published_scope: string;
    tags: string;
    admin_graphql_api_id: string;
    variants: ShopifyVariantResponse[];
  };
}

export class ShopifyVariantResponse {
  id: number;
  product_id: number;
  title: string;
  price: string;
  sku: string;
  position: number;
  inventory_policy: string;
  fulfillment_service: string;
  created_at: string;
  updated_at: string;
  taxable: boolean;
  barcode: string;
  requires_shipping: boolean;
}
