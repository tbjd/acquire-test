import { HttpException, Injectable } from '@nestjs/common';
import { Merchant } from '../../merchant/merchant';
import { ProductDto } from '../../product/dto/product-dto';
import { HttpService } from '@nestjs/axios';
import { catchError, map, Observable } from 'rxjs';
import { ShopifyMapperService } from './shopify-mapper.service';
import { ShopifyProductResponse } from './shopify-product-response';

@Injectable()
export class ShopifyApiService {
  public static readonly MERCHANT_NAME = 'shopify';

  constructor(
    private readonly httpService: HttpService,
    private readonly shopifyMapperService: ShopifyMapperService,
  ) {}

  findProduct(merchant: Merchant, productId: string): Observable<ProductDto> {
    return this.httpService
      .get<ShopifyProductResponse>(this.getUrl(merchant, productId))
      .pipe(
        map((response) =>
          this.shopifyMapperService.buildProduct(response.data),
        ),
        catchError((e) => {
          throw new HttpException(e?.response?.data, e?.response?.status);
        }),
      );
  }

  private getUrl(merchant: Merchant, productId: string) {
    return (
      'https://' +
      merchant.platform.auth.key +
      ':' +
      merchant.platform.auth.password +
      '@acquire-tech-test.myshopify.com/admin/api/2021-04/products/' +
      productId +
      '.json'
    );
  }
}
