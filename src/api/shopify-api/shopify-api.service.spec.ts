import { Test, TestingModule } from '@nestjs/testing';
import { ShopifyApiService } from './shopify-api.service';
import { ShopifyMapperService } from './shopify-mapper.service';
import { ApiModule } from '../api.module';
import { HttpModule, HttpService } from '@nestjs/axios';
import { of, throwError } from 'rxjs';
import { AxiosResponse } from 'axios';
import { ShopifyProductResponse } from './shopify-product-response';
import { ProductDto } from '../../product/dto/product-dto';

describe('ShopifyApiService', () => {
  let shopifyApiService: ShopifyApiService;
  let httpService: HttpService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ApiModule, HttpModule],
      providers: [
        ShopifyApiService,
        ShopifyMapperService,
        ShopifyMapperService,
      ],
    }).compile();

    shopifyApiService = module.get<ShopifyApiService>(ShopifyApiService);
    httpService = module.get<HttpService>(HttpService);
  });

  describe('When find product', () => {
    it('Should make a proper GET Request to Shopify API ', async () => {
      const response: AxiosResponse<ShopifyProductResponse> = {
        config: undefined,
        headers: undefined,
        status: 201,
        statusText: '',
        data: {},
      };
      const httpServiceSpy = jest
        .spyOn(httpService, 'get')
        .mockImplementation(() => of(response));
      shopifyApiService.findProduct(
        shopifyMerchant,
        shopifyMerchant.products[0].id,
      );
      expect(httpServiceSpy).toBeCalledWith(expectedUrl);
      expect(httpServiceSpy).toBeCalledTimes(1);
    });
    it('Should map the Shopify product to a ProductDTO ', (done) => {
      //This test should be done in shopify-mapper.service.spec
      const response: AxiosResponse<ShopifyProductResponse> = {
        config: undefined,
        headers: undefined,
        status: 201,
        statusText: '',
        data: shopifyProductResponse,
      };
      jest.spyOn(httpService, 'get').mockImplementation(() => of(response));
      shopifyApiService
        .findProduct(shopifyMerchant, shopifyMerchant.products[0].id)
        .subscribe((shopifyProductDto) => {
          expect(shopifyProductDto.id).toBe(
            shopifyProductResponse.product.id.toString(),
          );
          expect(shopifyProductDto.title).toBe(
            shopifyProductResponse.product.title,
          );
          expect(shopifyProductDto.description).toBe(
            shopifyProductResponse.product.body_html,
          );
          expect(shopifyProductDto.id).toBe(
            shopifyProductResponse.product.id.toString(),
          );
          expect(shopifyProductDto.merchantName).toBe(
            ShopifyApiService.MERCHANT_NAME,
          );
          expect(shopifyProductDto.variants[0].id).toBe(
            shopifyProductResponse.product.variants[0].id.toString(),
          );
          expect(shopifyProductDto.variants[0].price).toBe(
            shopifyProductResponse.product.variants[0].price,
          );
          expect(shopifyProductDto.variants[0].title).toBe(
            shopifyProductResponse.product.variants[0].title,
          );
          done();
        });
    });
    it('Should handle Shopify API errors', (done) => {
      const result = {
        response: {
          data: '',
          status: 404,
          statusText: 'string',
          headers: {},
          config: {},
        },
      };
      jest
        .spyOn(httpService, 'get')
        .mockImplementation(() => throwError(() => result));
      shopifyApiService
        .findProduct(shopifyMerchant, shopifyMerchant.products[0].id)
        .subscribe({
          error: (err) => {
            expect(err.status).toBe(result.response.status);
            done();
          },
        });
    });
  });
});
export const productDto: ProductDto = {
  id: '1',
  variants: [],
  title: '',
  merchantName: '',
  description: '',
};

export const shopifyMerchant = {
  merchant: 'Merchant 1',
  platform: {
    type: ShopifyApiService.MERCHANT_NAME,
    auth: {
      key: 'a_key',
      password: 'a_password',
    },
  },
  products: [
    {
      id: productDto.id,
    },
  ],
};
export const shopifyProductResponse = {
  product: {
    id: parseInt(productDto.id),
    title: 'a_title',
    body_html: 'mock',
    vendor: 'mock',
    product_type: 'mock',
    created_at: 'mock',
    handle: 'mock',
    updated_at: 'mock',
    published_at: 'mock',
    template_suffix: 'mock',
    status: 'mock',
    published_scope: 'mock',
    tags: 'mock',
    admin_graphql_api_id: 'mock',
    variants: [
      {
        id: 1,
        product_id: 1,
        title: 'mock',
        price: '5',
        sku: 'mock',
        position: 1,
        inventory_policy: 'mock',
        fulfillment_service: 'mock',
        created_at: 'mock',
        updated_at: 'mock',
        taxable: true,
        barcode: 'mock',
        requires_shipping: false,
      },
    ],
  },
};
export const expectedUrl =
  'https://' +
  shopifyMerchant.platform.auth.key +
  ':' +
  shopifyMerchant.platform.auth.password +
  '@acquire-tech-test.myshopify.com/admin/api/2021-04/products/' +
  shopifyMerchant.products[0].id +
  '.json';
