import { Injectable } from '@nestjs/common';
import {
  ShopifyProductResponse,
  ShopifyVariantResponse,
} from './shopify-product-response';
import { ProductDto } from '../../product/dto/product-dto';
import { ShopifyApiService } from './shopify-api.service';

@Injectable()
export class ShopifyMapperService {
  buildProduct(shopifyProductResponse: ShopifyProductResponse): ProductDto {
    return {
      id: shopifyProductResponse.product.id.toString(),
      description: shopifyProductResponse.product.body_html,
      merchantName: ShopifyApiService.MERCHANT_NAME,
      title: shopifyProductResponse.product.title,
      variants: this.getVariants(shopifyProductResponse.product.variants),
    };
  }

  private getVariants(shopifyVariantResponse: ShopifyVariantResponse[]) {
    return shopifyVariantResponse.map((shopifyVariant) => {
      return {
        id: shopifyVariant.id.toString(),
        title: shopifyVariant.title,
        price: shopifyVariant.price,
      };
    });
  }
}
