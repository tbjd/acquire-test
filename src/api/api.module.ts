import { Module } from '@nestjs/common';
import { SquareApiService } from './square-api/square-api.service';
import { ShopifyApiService } from './shopify-api/shopify-api.service';
import { ProductApiService } from './product-api.service';
import { HttpModule } from '@nestjs/axios';
import { ShopifyMapperService } from './shopify-api/shopify-mapper.service';
import { SquareApiMapperService } from './square-api/square-api-mapper.service';

@Module({
  imports: [HttpModule],
  providers: [
    SquareApiService,
    ShopifyApiService,
    ProductApiService,
    ShopifyMapperService,
    SquareApiMapperService,
  ],
  exports: [ProductApiService, SquareApiService, ShopifyApiService],
})
export class ApiModule {}
