export interface ProductDto {
  id: string;
  title: string;
  description: string;
  merchantName: string;
  variants: Variant[];
}

export interface Variant {
  id: string;
  title: string;
  price;
}
