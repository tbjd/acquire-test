import { Module } from '@nestjs/common';
import { ProductService } from './product.service';
import { MerchantModule } from '../merchant/merchant.module';
import { ProductController } from './product.controller';
import { MerchantService } from '../merchant/merchant.service';
import { ProductApiService } from '../api/product-api.service';
import { ApiModule } from '../api/api.module';

@Module({
  providers: [ProductService, MerchantService, ProductApiService],
  imports: [MerchantModule, ApiModule],
  controllers: [ProductController],
})
export class ProductModule {}
