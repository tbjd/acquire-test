import { Test, TestingModule } from '@nestjs/testing';
import { ProductService } from './product.service';
import { MerchantService } from '../merchant/merchant.service';
import { ProductModule } from './product.module';
import { ProductApiService } from '../api/product-api.service';
import { ProductDto } from './dto/product-dto';
import { ApiModule } from '../api/api.module';
import { of } from 'rxjs';

describe('ProductService', () => {
  let productService: ProductService;
  const productApiService = { findProduct: () => of(productDto) };
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ProductModule, ApiModule],
      providers: [ProductService, MerchantService],
    })
      .overrideProvider(ProductApiService)
      .useValue(productApiService)
      .compile();
    productService = module.get<ProductService>(ProductService);
  });
  describe('findAll', () => {
    it('should return the products of all merchants', (done) => {
      productService.findAll().subscribe((value) => {
        expect(value).toEqual([productDto, productDto]);
        done();
      });
    });
  });
});

export const productDto: ProductDto = {
  id: '1',
  variants: [],
  title: '',
  merchantName: '',
  description: '',
};
