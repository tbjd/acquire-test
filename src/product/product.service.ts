import { Injectable } from '@nestjs/common';
import { MerchantService } from '../merchant/merchant.service';
import { forkJoin, mergeMap, Observable } from 'rxjs';
import { ProductApiService } from '../api/product-api.service';
import { ProductDto } from './dto/product-dto';

@Injectable()
export class ProductService {
  constructor(
    private readonly merchantService: MerchantService,
    private readonly productApiService: ProductApiService,
  ) {}

  findAll(): Observable<ProductDto[]> {
    return this.merchantService.findAll().pipe(
      mergeMap((merchants) => {
        return forkJoin(
          merchants.reduce((acc, merchant) => {
            return acc.concat(
              merchant.products.map((product) => {
                return this.productApiService.findProduct(merchant, product.id);
              }),
            );
          }, []),
        );
      }),
    );
  }
}
