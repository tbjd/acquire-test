export class Merchant {
  merchant: string;
  platform: MerchantPlatform;
  products: MerchantProduct[];
}

export class MerchantPlatform {
  type: string;
  auth: MerchantPlatformAuth;
}

export class MerchantPlatformAuth {
  key?: string;
  password?: string;
  token?: string;
}

export class MerchantProduct {
  id: string;
}
