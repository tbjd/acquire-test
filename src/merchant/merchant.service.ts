import { Injectable } from '@nestjs/common';
import { Merchant } from './merchant';
import { Observable, of } from 'rxjs';
import { merchantsDataMock } from './merchants-data-mock';

@Injectable()
export class MerchantService {
  findAll(): Observable<Merchant[]> {
    return of(merchantsDataMock);
  }
}
