import { Module } from '@nestjs/common';
import { MerchantModule } from './merchant/merchant.module';
import { ProductModule } from './product/product.module';
import { ApiModule } from './api/api.module';

@Module({
  imports: [MerchantModule, ProductModule, ApiModule],
})
export class AppModule {}
